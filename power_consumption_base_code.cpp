#include <cassert>
#include <cmath>
#include <functional>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

// BOILERPLATE CODE [Don't edit!]
/////////////////////////////////////////////////////////////
#define CHECK(f)          \
    do                    \
    {                     \
        if (!(f))         \
            return false; \
        break;            \
    }                     \
    while (0);

#define TEST(f) test_runner(f, #f)

void test_runner(std::function<bool()>&& p_func, std::string const& p_message)
{
    bool result = true;
    try
    {
        result = p_func();
    }
    catch (...)
    {
        result = false;
    }
    !result ? std::cout << "[ERROR] " : std::cout << "[OK]    ";
    std::cout << p_message << "\n";
}

template<class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& dt)
{
    std::cout << " [";
    for (auto& it : dt)
    {
        std::cout << " " << it;
    }
    std::cout << " ]";
    return os;
}

// Base  [Don't edit!]
/////////////////////////////////////////////////////////////
enum class Unit
{
    kWh,
    Liters,
};

std::ostream& operator<<(std::ostream& os, const Unit& unit)
{
    switch (unit)
    {
    case Unit::kWh: os << " kWh"; break;
    case Unit::Liters: os << " l"; break;
    }
    return os;
}

struct Capacity
{
    float value;
    Unit unit;

    bool operator==(Capacity const& p_other) const
    {
        return unit == p_other.unit && fabs(p_other.value - value) < 0.001f;
    }
};

std::ostream& operator<<(std::ostream& os, const Capacity& dt)
{
    os << dt.value << dt.unit;
    return os;
}

struct Segment
{
    float length_km;
    float speed_kmh;
};
using Path = std::vector<Segment>;

class IVehicle
{
public:
    virtual std::string get_model() const noexcept = 0;

    virtual void drive(Path const& p_path) = 0;

    virtual std::vector<Capacity> get_remaining_capacity() const noexcept = 0;
};

/*
 __      __        .__  __           .__                        ._.
/  \    /  \_______|__|/  |_  ____   |  |__   ___________   ____| |
\   \/\/   /\_  __ \  \   __\/ __ \  |  |  \_/ __ \_  __ \_/ __ \ |
 \        /  |  | \/  ||  | \  ___/  |   Y  \  ___/|  | \/\  ___/\|
  \__/\  /   |__|  |__||__|  \___  > |___|  /\___  >__|    \___  >_
       \/                        \/       \/     \/            \/\/
                                                        Good luck!
*/

struct SpeedConsumption {
    float speed;
    float consumption;
    Unit unit;
};

/******* MOMENTO DE ILUMINACIÓN (A BUENAS HORAS) ******
 * Tras hacer ElectricVehicle iba a cambiarlo por OneMotorVehicle para
 * poder reutilizar la clase para el vehiculo de gasolina...
 * 
 * Y me he dado cuenta de que puedo hacer Vehicle compatible con los tres y ya está.
 * 
 */
 
class Vehicle : public IVehicle
{
public:
    // Constructor for hybrid vehicles
    Vehicle(std::string const& p_model, Capacity const& p_electric_capacity, Capacity p_gasoline_capacity) : model(p_model) {
        remaining_capacity.emplace_back(p_electric_capacity);
        remaining_capacity.emplace_back(p_gasoline_capacity);

        // This could be done in a different way (maintaining all six speeds/consumptions) to ensure that the hybrid vehicles
        // could still run using the other motor if one is empty, but I'll do this to simplify my life.
        consumption = {
            SpeedConsumption{50.0, 10.0, Unit::kWh},
            SpeedConsumption{80.0, 6.0, Unit::Liters},
            SpeedConsumption{120.0, 12.0, Unit::Liters}
        };
    };

    //Constructor for electric/gasoline vehicles
    Vehicle(std::string const& p_model, Capacity const& p_capacity) : model(p_model) {
        remaining_capacity.emplace_back(p_capacity);
        if(p_capacity.unit==Unit::kWh){
            consumption = {
                SpeedConsumption{50.0, 10.0, Unit::kWh},
                SpeedConsumption{80.0, 13.0, Unit::kWh},
                SpeedConsumption{120.0, 15.0, Unit::kWh}
            };
        }
        else
        {
            consumption = {
                SpeedConsumption{50.0, 7.0, Unit::Liters},
                SpeedConsumption{80.0, 6.0, Unit::Liters},
                SpeedConsumption{120.0, 12.0, Unit::Liters}
            };
        }
    };

    std::string get_model() const noexcept {
        return model;
    }
    
    std::vector<Capacity> get_remaining_capacity() const noexcept{
        return remaining_capacity;
    }

    void drive(Path const& p_path){
        // assuming speeds in segments can only be equal to consumptions defined: 50.0, 80.0, 120.0
        for(auto const& seg : p_path){
            // travel each segment:
            // look for consumption for current speed (seg.speed_kmh)
            
            auto iterCons = std::find_if(consumption.begin(), consumption.end(),
                        [&seg](SpeedConsumption const & speedcons) {return (fabs(speedcons.speed - seg.speed_kmh) < 0.001f);});

            if ( iterCons != consumption.end() )
            {
                float energyNeeded = iterCons->consumption * (seg.length_km / 100.0);

                auto iterCap = std::find_if(remaining_capacity.begin(), remaining_capacity.end(),
                        [iterCons](Capacity const & cap) {return (cap.unit==iterCons->unit);});

                if (iterCap != remaining_capacity.end()){
                    iterCap->value = (energyNeeded < iterCap->value)?
                    (iterCap->value - energyNeeded) : (0.0f);
                }
                    
                if(iterCap->value < 0.001f){
                    // had to look it up: https://stackoverflow.com/questions/8480640/how-to-throw-a-c-exception
                    throw std::out_of_range( "Not enough capacity to finish the segment" );
                }
            }
        }
    }

private:
    std::string model;
    std::vector<Capacity> remaining_capacity;
    std::vector<SpeedConsumption> consumption;
};

std::shared_ptr<IVehicle> make_vehicle_electric(std::string const& p_model, Capacity const& p_electricity)
{
    // Google: c++ shared_ptr of interface -> https://stackoverflow.com/questions/34652117/use-an-interface-as-shared-pointer-parameter
    return std::shared_ptr<IVehicle>(new Vehicle(p_model, p_electricity));
}

std::shared_ptr<IVehicle> make_vehicle_gasoline(std::string const& p_model, Capacity const& p_gasoline)
{
    return std::shared_ptr<IVehicle>(new Vehicle(p_model, p_gasoline));
}

std::shared_ptr<IVehicle> make_vehicle_hybrid(std::string const& p_model,
                                              Capacity const& p_electricity,
                                              Capacity const& p_gasoline)
{
    /**
     *     return std::shared_ptr<IVehicle>(new Vehicle(p_model, p_gasoline, p_electricity));
     * 
     * I believe the above construction/implementation to be correct, but it does not pass your test.
     * Why would you expect the remaining_capacity result to be in a certain order?
     * 
     */

    return std::shared_ptr<IVehicle>(new Vehicle(p_model, p_electricity, p_gasoline));
}

// TESTS [Don't edit!]
/////////////////////////////////////////////////////////////
bool test_drive(std::shared_ptr<IVehicle> p_vehicle, Path const& p_path)
{
    assert(p_vehicle);
    try
    {
        p_vehicle->drive(p_path);
    }
    catch (...)
    {
        return false;
    }
    return true;
}

bool test_gasoline_expect_complete_route()
{
    auto path    = Path{Segment{10, 50}, Segment{70, 120}, Segment{200, 80}};
    auto vehicle = make_vehicle_gasoline("BMW X5", Capacity{60, Unit::Liters});
    CHECK(vehicle);

    CHECK(test_drive(vehicle, path));
    auto const capacity = vehicle->get_remaining_capacity();

    std::cout << "Remaining capacity: " << vehicle->get_remaining_capacity() << '\n';
    CHECK((capacity == std::vector<Capacity>{Capacity{38.9, Unit::Liters}}));

    return true;
}

bool test_hybrid_expect_complete_route()
{
    auto path    = Path{Segment{10, 50}, Segment{70, 120}, Segment{200, 80}};
    auto vehicle = make_vehicle_hybrid("Kia Soul", Capacity{30, Unit::kWh}, Capacity{25, Unit::Liters});
    CHECK(vehicle);

    CHECK(test_drive(vehicle, path));
    auto const capacity = vehicle->get_remaining_capacity();

    std::cout << "Remaining capacity: " << vehicle->get_remaining_capacity() << '\n';
    CHECK((capacity == std::vector<Capacity>{Capacity{29, Unit::kWh}, Capacity{4.6, Unit::Liters}}));

    return true;
}

bool test_electric_expect_complete_route()
{
    auto path    = Path{Segment{10, 50}, Segment{70, 120}, Segment{200, 80}};
    auto vehicle = make_vehicle_electric("Tesla", Capacity{80, Unit::kWh});
    CHECK(vehicle);

    CHECK(test_drive(vehicle, path));
    auto const capacity = vehicle->get_remaining_capacity();

    std::cout << "Remaining capacity: " << vehicle->get_remaining_capacity() << '\n';
    CHECK((capacity == std::vector<Capacity>{Capacity{42.5, Unit::kWh}}));

    return true;
}

bool test_electric_expect_doesnt_complete_route()
{
    auto path    = Path{Segment{400, 50}};
    auto vehicle = make_vehicle_electric("Tesla", Capacity{35, Unit::kWh});
    CHECK(vehicle);

    CHECK(!test_drive(vehicle, path));

    return true;
}

int main()
{
    TEST(test_gasoline_expect_complete_route);
    TEST(test_hybrid_expect_complete_route);
    TEST(test_electric_expect_complete_route);
    TEST(test_electric_expect_doesnt_complete_route);
}
